/*
	Copyright 2017 Alessandro Pellizzari

	This file is part of stem.

	Stem is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 2.

	Stem is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Stem. If not, see <http://www.gnu.org/licenses/>.
*/

#![feature(global_allocator)]
#![feature(allocator_api)]

use std::heap::System;

#[global_allocator]
static ALLOCATOR: System = System;

extern crate isatty;
extern crate getopts;

use std::env;
use std::process;
use std::io::{self, Write};
use getopts::Options;

fn main() {
	let args: Vec<String> = env::args().skip(1).collect();

	let mut opts = Options::new();
	opts.optflag("h", "help", "print this help menu");
	opts.optflag("p", "path", "remove the path from the file name");
	let matches = opts.parse(&args)
		.unwrap_or_else(|f| { err(&f.to_string(), 1); });

	if matches.opt_present("h") {
		print_usage("stem", opts);
		process::exit(0);
	}

	let remove_path = matches.opt_present("p");

	if matches.free.is_empty() {
		err("File name missing. Please provide a file name to stem", 2);
	}

	let file_name = &matches.free[0];

	let last_slash = file_name.rfind("/").unwrap_or_default();
	file_name.rfind(".")
		.or_else(|| Some(file_name.len()))
		.map(|end| if end > last_slash { end } else { file_name.len() })
		.map(|end| {
			let start = if remove_path { last_slash + 1 } else { 0 };

			let res = &file_name[start..end];
			if res.ends_with("/") {
				err("Stripping extension results in a directory", 3);
			}
			let mut stdout = io::stdout();
			let _ = stdout.write(res.as_bytes());

			if isatty::stdout_isatty() {
				let _ = stdout.write("\n".as_bytes());
			}
		});
}

fn err(msg: &str, code: i32) -> ! {
	let mut stderr = io::stderr();
	let _ = stderr.write(msg.as_bytes());
	process::exit(code);
}

fn print_usage(program: &str, opts: Options) {
	let brief = format!("Usage: {} [options] FILENAME", program);
	print!("{}", opts.usage(&brief));
}
