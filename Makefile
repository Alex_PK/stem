target/release/stem: src/main.rs
	rustup run nightly cargo build --release
	strip target/release/stem

install: target/release/stem
	cp target/release/stem /usr/local/bin/stem

clean:
	cargo clean
